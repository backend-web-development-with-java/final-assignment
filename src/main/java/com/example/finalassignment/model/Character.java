package com.example.finalassignment.model;


import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

// Entity class that initializes the table for characters with associated fields
@Entity
@Table(name = "CharacterTable")
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String fullName;
    private String alias;
    private String gender;
    private String picture;


    // relationship to movies
    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies = new HashSet<>();

    // Create empty constructor
    public Character(){

    }

    // create constructor
    public Character(String fullName, String alias){
        this.fullName = fullName;
        this.alias = alias;
    }

    // getters and setters for the encapsulated fields
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void addMovie(Movie movie){
        this.movies.add(movie);
        movie.getCharacters().add(this);
    }
}