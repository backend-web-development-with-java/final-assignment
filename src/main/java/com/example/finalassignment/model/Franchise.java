package com.example.finalassignment.model;

import jakarta.persistence.*;

import java.util.List;

// Entity class that initializes the table for franchises with associated fields
@Entity
@Table(name ="FranchiseTable")
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String Name;
    private String Description;

    // relationship to movies
    @OneToMany(mappedBy = "franchise", cascade = CascadeType.ALL)
    private List<Movie> movies;


    // getters and setters for the fields
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }


//    public void setMovies(List<Movie> movies){
//        this.movies = movies;
//        for (Movie movie : movies){
//            movie.setFranchise(this);
//        }
//    }
}