package com.example.finalassignment.model;

import jakarta.persistence.*;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


// Entity class that initializes the table for movies with associated fields
@Entity
@Table(name ="MovieTable")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String movieTitle;
    private String genre;
    private String releaseYear;
    private String director;
    private String picture;
    private String trailer;

    // relationship with franchise
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

    // relationship with characters
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "movie_character",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "character_id"))
    private Set<Character> characters = new HashSet<>();


    // empty constructor method
    public Movie(){

    }

    // Constructor method
    public Movie(String movieTitle, String genre){
        this.movieTitle = movieTitle;
        this.genre = genre;
    }

    // getters and setters for the encapsulated fields
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }


    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public Long getFranchiseId() {
        return franchise != null ? franchise.getId() : null;
    }

    public void setFranchise(Franchise franchise){
        this.franchise = franchise;
    }

    public void addCharacter(Character character){
        this.characters.add(character);
        character.getMovies().add(this);
    }

    public Set<Character> getCharacters() {
        return characters;
    }

//    public void setFranchiseId(Long franchiseId) {
//        this.franchiseId = franchiseId;
//    }
}










