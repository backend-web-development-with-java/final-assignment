package com.example.finalassignment.model.dtos;

import java.util.List;

//create a DTO class in order to not over-post output of CRUD operations
public class CharacterDTO {
    private Long id;
    private String fullName;
    private String alias;
    private String gender;
    private String picture;
    private List<Long> movieIds;

    // constructor method
    public CharacterDTO(Long id, String fullName, String alias, String gender, String picture, List<Long> movieIds) {
        this.id = id;
        this.fullName = fullName;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
        this.movieIds = movieIds;
    }

    // getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<Long> getMovieIds() {
        return movieIds;
    }

    public void setMovieIds(List<Long> movieIds) {
        this.movieIds = movieIds;
    }
}