package com.example.finalassignment.model.dtos;

import com.example.finalassignment.model.Movie;

import java.util.List;

//create a DTO class in order to not over-post output of CRUD operations
public class FranchiseDTO {

    private Long id;
    private String Name;
    private String Description;
    private List<Movie> movieIds;

    // Constructor method
    public FranchiseDTO(Long id, String name, String description, List<Movie> movieIds) {
        this.id = id;
        this.Name = name;
        this.Description = description;
        this.movieIds = movieIds;
    }

    // getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }


    public List<Movie> getMovieIds() {
        return movieIds;
    }

    public void setMovieIds(List<Movie> movieIds) {
        this.movieIds = movieIds;
    }
}