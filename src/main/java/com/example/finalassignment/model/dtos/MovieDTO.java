package com.example.finalassignment.model.dtos;

import java.util.List;


//create a DTO class in order to not over-post output of CRUD operations
public class MovieDTO {

    private Long id;
    private String movieTitle;
    private String genre;
    private String releaseYear;
    private String director;
    private String picture;
    private String trailer;
    private Long franchiseId;
    private List<Long> characterIds;


    // constructor method
    public MovieDTO(Long id, String movieTitle, String genre, String releaseYear, String director, String picture, String trailer, Long franchiseId, List<Long> characterIds) {
        this.id = id;
        this.movieTitle = movieTitle;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.picture = picture;
        this.trailer = trailer;
        this.franchiseId = franchiseId;
        this.characterIds = characterIds;

    }

    // getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public Long getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(Long franchiseId) {
        this.franchiseId = franchiseId;
    }

    public List<Long> getCharacterIds() {
        return characterIds;
    }

    public void setCharacterIds(List<Long> characterIds) {
        this.characterIds = characterIds;
    }
}
