package com.example.finalassignment.Controllers;

import com.example.finalassignment.Mappers.CharacterMapper;
import com.example.finalassignment.Mappers.FranchiseMapper;
import com.example.finalassignment.Repository.CharacterRepository;
import com.example.finalassignment.Repository.FranchiseRepository;
import com.example.finalassignment.Services.CharacterService;
import com.example.finalassignment.Services.FranchiseService;
import com.example.finalassignment.model.Character;
import com.example.finalassignment.model.Franchise;
import com.example.finalassignment.model.dtos.CharacterDTO;
import com.example.finalassignment.model.dtos.FranchiseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;


// create a class that handles the CRUD functionality for Characters
@RestController
@RequestMapping("/franchise")
public class FranchiseController {

    // make private finals for service, mapper and repo
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final FranchiseRepository franchiseRepository;

    // constructor method
    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, FranchiseRepository franchiseRepository) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.franchiseRepository = franchiseRepository;
    }

    // get franchise by ID
    @GetMapping(value = "getFranchise/{id}", produces = "application/json")
    public ResponseEntity<FranchiseDTO> getFranchise(@PathVariable Long id) {
        try{
            Franchise franchise = franchiseService.getFranchiseById(id);
            FranchiseDTO franchiseDto = franchiseMapper.toDto(franchise);
            return ResponseEntity.ok(franchiseDto);
        } catch (ResponseStatusException ex){
            return ResponseEntity.status(ex.getStatusCode()).build();
        }
    }

    // add a new franchise by inserting values in each field in the DTO
    @PostMapping("createFranchise/")
    public ResponseEntity<FranchiseDTO> addFranchise(@RequestBody FranchiseDTO franchiseDTO) {
        Franchise franchise = franchiseMapper.toEntity(franchiseDTO);
        Franchise savedFranchise = franchiseService.createFranchise(franchise);
        FranchiseDTO savedFranchiseDTO = franchiseMapper.toDto(savedFranchise);
        URI location = URI.create("franchise/" + savedFranchise.getId());
        return ResponseEntity.created(location).body(savedFranchiseDTO);
    }

    // update a franchise
    @PutMapping("updateFranchise/{id}")
    public ResponseEntity<FranchiseDTO> updateFranchise(@PathVariable Long id, @RequestBody FranchiseDTO franchiseDTO) {
        Franchise existingFranchise = franchiseService.getFranchiseById(id);
        Franchise updatedFranchise = franchiseMapper.toEntity(franchiseDTO);
        updatedFranchise.setId(existingFranchise.getId()); // ensure the updated franchise has the correct ID
        Franchise savedFranchise = franchiseRepository.save(updatedFranchise);
        FranchiseDTO savedFranchiseDTO = franchiseMapper.toDto(savedFranchise);
        return ResponseEntity.ok(savedFranchiseDTO);
    }


    // delete a franchise
    @DeleteMapping(value = "deleteFranchise/{id}", produces = "application/json")
    public ResponseEntity<Void> deleteFranchise(@PathVariable Long id){
        try {
            franchiseService.deleteFranchiseById(id);
            return ResponseEntity.noContent().build();
        } catch (ResponseStatusException ex) {
            return ResponseEntity.status(ex.getStatusCode()).build();
        }
    }
}