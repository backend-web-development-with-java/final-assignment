//
// Application didn't run at the point of delivery of the assignment, therefore it is commented out.
//
//package com.example.finalassignment.Controllers;
//
//import com.example.finalassignment.Mappers.MovieMapper;
//import com.example.finalassignment.Repository.MovieRepository;
//import com.example.finalassignment.Services.CharacterService;
//import com.example.finalassignment.Services.MovieService;
//import com.example.finalassignment.model.Character;
//import com.example.finalassignment.model.Movie;
//import com.example.finalassignment.model.dtos.CharacterDTO;
//import com.example.finalassignment.model.dtos.MovieDTO;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.server.ResponseStatusException;
//
//import java.net.URI;
//
//@RestController
//@RequestMapping("/movie")
//public class MovieController {
//
//    private final MovieService movieService;
//    private final MovieMapper movieMapper;
//    private final MovieRepository movieRepository;
//
//    public MovieController(MovieService movieService, MovieMapper movieMapper, MovieRepository movieRepository){
//        this.movieService = movieService;
//        this.movieMapper = movieMapper;
//        this.movieRepository = movieRepository;
//    }
//
//    @GetMapping(value = "getMovie/{id}", produces = "application/json")
//    public ResponseEntity<MovieDTO> getMovie(@PathVariable Long id) {
//        try{
//            Movie movie = movieService.getMovieById(id);
//            MovieDTO movieDto = movieMapper.toDto(movie);
//            return ResponseEntity.ok(movieDto);
//        } catch (ResponseStatusException ex){
//            return ResponseEntity.status(ex.getStatusCode()).build();
//        }
//    }
//
//    @PostMapping("createMovie/")
//    public ResponseEntity<MovieDTO> addMovie(@RequestBody MovieDTO movieDTO) {
//        Movie movie = movieMapper.toEntity(movieDTO);
//        Movie savedMovie = movieService.createMovie(movie);
//        MovieDTO savedMovieDTO = movieMapper.toDto(savedMovie);
//        URI location = URI.create("movie/" + savedMovie.getId());
//        return ResponseEntity.created(location).body(savedMovieDTO);
//    }
//
//    @PutMapping("updateMovie/{id}")
//    public ResponseEntity<MovieDTO> updateMovie(@PathVariable Long id, @RequestBody MovieDTO movieDTO) {
//        Movie existingMovie = movieService.getMovieById(id);
//        Movie updatedMovie = movieMapper.toEntity(movieDTO);
//        updatedMovie.setId(existingMovie.getId()); // ensure the updated Movie has the correct ID
//        Movie savedMovie = movieRepository.save(updatedMovie);
//        MovieDTO savedMovieDTO = movieMapper.toDto(savedMovie);
//        return ResponseEntity.ok(savedMovieDTO);
//    }
//
//    @DeleteMapping(value = "deleteMovie/{id}", produces = "application/json")
//    public ResponseEntity<Void> deleteMovie(@PathVariable Long id){
//        try {
//            movieService.deleteMovieById(id);
//            return ResponseEntity.noContent().build();
//        } catch (ResponseStatusException ex) {
//            return ResponseEntity.status(ex.getStatusCode()).build();
//        }
//    }
//}