package com.example.finalassignment.Controllers;


import com.example.finalassignment.Mappers.CharacterMapper;
import com.example.finalassignment.Repository.CharacterRepository;
import com.example.finalassignment.Services.CharacterService;
import com.example.finalassignment.model.Character;
import com.example.finalassignment.model.dtos.CharacterDTO;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import java.net.URI;
import java.sql.ClientInfoStatus;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


// create a class that handles the CRUD functionality for Characters
@RestController
@RequestMapping("/character")
public class CharacterController {

        // make private finals for service, mapper and repo
        private final CharacterService characterService;
        private final CharacterMapper characterMapper;
        private final CharacterRepository characterRepository;

        // constructor method
        public CharacterController(CharacterService characterService, CharacterMapper characterMapper, CharacterRepository characterRepository) {
            this.characterService = characterService;
            this.characterMapper = characterMapper;
            this.characterRepository = characterRepository;
        }

        // get character by ID
        @GetMapping(value = "getCharacter/{id}", produces = "application/json")
        public ResponseEntity<CharacterDTO> getCharacter(@PathVariable Long id) {
            try{
                Character character = characterService.getCharacterById(id);
                CharacterDTO characterDto = characterMapper.toDto(character);
                return ResponseEntity.ok(characterDto);
            } catch (ResponseStatusException ex){
                return ResponseEntity.status(ex.getStatusCode()).build();
            }
        }

        // add a new character by inserting values in each field in the DTO
        @PostMapping("createCharacter/")
        public ResponseEntity<CharacterDTO> addCharacter(@RequestBody CharacterDTO characterDTO) {
            Character character = characterMapper.toEntity(characterDTO);
            Character savedCharacter = characterService.createCharacter(character);
            CharacterDTO savedCharacterDTO = characterMapper.toDto(savedCharacter);
            URI location = URI.create("character/" + savedCharacter.getId());
            return ResponseEntity.created(location).body(savedCharacterDTO);
        }

        // update a character
        @PutMapping("updateCharacter/{id}")
        public ResponseEntity<CharacterDTO> updateCharacter(@PathVariable Long id, @RequestBody CharacterDTO characterDTO) {
            Character existingCharacter = characterService.getCharacterById(id);
            Character updatedCharacter = characterMapper.toEntity(characterDTO);
            updatedCharacter.setId(existingCharacter.getId()); // ensure the updated character has the correct ID
            Character savedCharacter = characterRepository.save(updatedCharacter);
            CharacterDTO savedCharacterDTO = characterMapper.toDto(savedCharacter);
            return ResponseEntity.ok(savedCharacterDTO);
        }


        // delete a character
        @DeleteMapping(value = "deleteCharacter/{id}", produces = "application/json")
        public ResponseEntity<Void> deleteCharacter(@PathVariable Long id){
            try {
                characterService.deleteCharacterById(id);
                return ResponseEntity.noContent().build();
            } catch (ResponseStatusException ex) {
                return ResponseEntity.status(ex.getStatusCode()).build();
            }
        }
}