package com.example.finalassignment.Repository;


import org.springframework.stereotype.Repository;
import com.example.finalassignment.model.Character;
import org.springframework.data.jpa.repository.JpaRepository;

// create a repository interface for characters that inherits the JpaRepo
@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {

}