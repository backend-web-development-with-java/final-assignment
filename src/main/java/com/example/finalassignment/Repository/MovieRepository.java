package com.example.finalassignment.Repository;


import org.springframework.stereotype.Repository;
import com.example.finalassignment.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;


// create a repository interface for movies that inherits the JpaRepo
@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
}