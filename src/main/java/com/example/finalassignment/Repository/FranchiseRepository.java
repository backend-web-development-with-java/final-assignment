package com.example.finalassignment.Repository;


import com.example.finalassignment.model.Franchise;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;


// create a repository interface for franchises that inherits the JpaRepo
@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}