package com.example.finalassignment.Services;


import com.example.finalassignment.Mappers.CharacterMapper;
import com.example.finalassignment.Repository.CharacterRepository;
import com.example.finalassignment.model.Character;
import com.example.finalassignment.model.dtos.CharacterDTO;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.webjars.NotFoundException;

import java.util.Optional;
import java.util.OptionalInt;

@Service
public class CharacterService {

    // initialize autowiring of Repo and Mapper
    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private CharacterMapper characterMapper;

    // get a character by supplied id
    public Character getCharacterById(Long id){
        Optional<Character> optionalCharacter = characterRepository.findById(id);
        return optionalCharacter.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Character not found"));
    }

    // create a new character
    public Character createCharacter(Character character){
        Character savedCharacter = characterRepository.save(character);
        return savedCharacter;
    }

    // delete an existing character
    public void deleteCharacterById(Long id){
        Optional<Character> optionalCharacter = characterRepository.findById(id);
        Character character = optionalCharacter.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Character not found"));
        characterRepository.delete(character);
    }
}