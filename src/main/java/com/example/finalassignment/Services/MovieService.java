//package com.example.finalassignment.Services;
//
//import com.example.finalassignment.Mappers.CharacterMapper;
//import com.example.finalassignment.Mappers.MovieMapper;
//import com.example.finalassignment.Repository.MovieRepository;
//import com.example.finalassignment.model.Character;
//import com.example.finalassignment.model.Movie;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//import org.springframework.web.server.ResponseStatusException;
//
//import java.util.Optional;
//
//@Service
//public class MovieService {
//
//    @Autowired
//    private MovieRepository movieRepository;
//
//    @Autowired
//    private MovieMapper movieMapper;
//
//    public Movie getMovieById(Long id){
//        Optional<Movie> optionalMovie = movieRepository.findById(id);
//        return optionalMovie.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Movie not found"));
//    }
//
//    public Movie createMovie(Movie movie){
//        Movie savedMovie = movieRepository.save(movie);
//        return savedMovie;
//    }
//
//    public void deleteMovieById(Long id){
//        Optional<Movie> optionalMovie = movieRepository.findById(id);
//        Movie movie = optionalMovie.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Character not found"));
//        movieRepository.delete(movie);
//    }
//
//
//}