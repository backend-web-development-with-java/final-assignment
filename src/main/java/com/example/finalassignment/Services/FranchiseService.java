package com.example.finalassignment.Services;

import com.example.finalassignment.Mappers.FranchiseMapper;
import com.example.finalassignment.Repository.FranchiseRepository;
import com.example.finalassignment.Repository.MovieRepository;
import com.example.finalassignment.model.Franchise;
import com.example.finalassignment.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class FranchiseService {

    // Initialize autowiring for Repo and Mapper
    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private FranchiseMapper franchiseMapper;

    // get a franchise by supplied ID
    public Franchise getFranchiseById(Long id){
        Optional<Franchise> optionalFranchise = franchiseRepository.findById(id);
        return optionalFranchise.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Franchise not found"));
    }

    // create a new franchise
    public Franchise createFranchise(Franchise franchise){
        Franchise savedFranchise = franchiseRepository.save(franchise);
        return savedFranchise;
    }

    // delete a franchise by supplied ID
    public void deleteFranchiseById(Long id){
        Optional<Franchise> optionalFranchise = franchiseRepository.findById(id);
        Franchise franchise = optionalFranchise.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Character not found"));
        franchiseRepository.delete(franchise);
    }

}
