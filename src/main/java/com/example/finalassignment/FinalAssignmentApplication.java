package com.example.finalassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

// TODO: Fix characterFunctionality make full CRUD

@SpringBootApplication
@EntityScan("com.example.finalassignment.model")
public class FinalAssignmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinalAssignmentApplication.class, args);
    }

}
