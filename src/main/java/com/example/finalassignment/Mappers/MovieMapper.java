
// code is commented out due to not making the application run at the time of delivery of assignment
// some errors will probably be in the code below, but they werent found before delivery of the task.

//package com.example.finalassignment.Mappers;
//
//import com.example.finalassignment.Services.CharacterService;
//import com.example.finalassignment.model.Character;
//import com.example.finalassignment.model.Franchise;
//import com.example.finalassignment.model.Movie;
//import com.example.finalassignment.model.dtos.CharacterDTO;
//import com.example.finalassignment.model.dtos.MovieDTO;
//import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//import org.mapstruct.factory.Mappers;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//@Mapper(componentModel = "spring")
//@Component
//public interface MovieMapper {
//
//
//
//    MovieMapper INSTANCE = Mappers.getMapper(MovieMapper.class);
//
//    @Mapping(source = "characters", target = "characterIds")
//    @Mapping(source = "franchiseId", target = "franchiseId")
//    MovieDTO toDto(Movie movie);
//
//    @Mapping(source = "characterIds", target = "characters")
//    @Mapping(source = "franchiseId", target = "franchiseId")
//    Movie toMovie(MovieDTO movieDTO);
//
//    default List<MovieDTO> toDtoList(List<Movie> movies){
//        return movies.stream()
//                .map(this::toDto)
//                .collect(Collectors.toList());
//    }
//
//    default List<Long> mapCharactersToIds(Set<Character> characters){
//        return characters.stream()
//                .map(Character::getId)
//                .collect(Collectors.toList());
//    }
//
//    default Long mapFranchiseToId(Franchise franchise){
//        return franchise != null ? franchise.getId() : null;
//    }
//
//    default Movie toEntity(MovieDTO movieDTO){
//        Movie movie = new Movie();
//        movie.setMovieTitle(movieDTO.getMovieTitle());
//        movie.setGenre(movieDTO.getGenre());
//        movie.setReleaseYear(movieDTO.getReleaseYear());
//        movie.setDirector(movieDTO.getDirector());
//        movie.setPicture(movieDTO.getPicture());
//        movie.setTrailer(movieDTO.getTrailer());
//        return movie;
//    }
//
//
//
//}
//
//public class MovieMapper {
//    public Movie mapToMovie(MovieDTO movieDTO) {
//        Movie movie = new Movie();
//        movie.setMovieTitle(movieDTO.getMovieTitle());
//        movie.setReleaseYear(movieDTO.getReleaseYear());
//        movie.addCharacter(new HashSet<>());
//        for (Long characterId : movieDTO.getCharacterIds()) {
//            Character character = new Character();
//            character.setId(characterId);
//            movie.getCharacters().add(character);
//        }
//        return movie;
//    }
//
//    public MovieDTO mapToMovieDTO(Movie movie) {
//        MovieDTO movieDTO = new MovieDTO();
//        movieDTO.setId(movie.getId());
//        movieDTO.setMovieTitle(movie.getMovieTitle());
//        movieDTO.setReleaseYear(movie.getReleaseYear());
//        movieDTO.setCharacterIds(new HashSet<>());
//        for (Character character : movie.getCharacters()) {
//            movieDTO.getCharacterIds().add(character.getId());
//        }
//        return movieDTO;
//    }
//}