package com.example.finalassignment.Mappers;


import com.example.finalassignment.model.Character;
import com.example.finalassignment.model.Movie;
import com.example.finalassignment.model.dtos.CharacterDTO;
import org.mapstruct.Mapping;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


// create a mapper interface using mapstruct and dtos
@Mapper(componentModel = "spring")
@Component
public interface CharacterMapper {

    CharacterMapper INSTANCE = Mappers.getMapper(CharacterMapper.class);


    // map the corresponding fields
    @Mapping(source = "movies", target = "movieIds")
    CharacterDTO toDto(Character character);


    // default methods for mapping
    default List<CharacterDTO> toDtoList(List<Character> characters){
        return characters.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    default List<Long> map(Set<Movie> value){
        return value.stream()
                .map(Movie::getId)
                .collect(Collectors.toList());
    }

    default Character toEntity(CharacterDTO characterDTO){
        Character character = new Character();
        character.setFullName(characterDTO.getFullName());
        character.setAlias(characterDTO.getAlias());
        character.setGender(characterDTO.getGender());
        character.setPicture(characterDTO.getPicture());
        return character;
    }
}