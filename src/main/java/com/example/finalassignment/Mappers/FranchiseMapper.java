package com.example.finalassignment.Mappers;

import com.example.finalassignment.model.Franchise;
import com.example.finalassignment.model.Movie;
import com.example.finalassignment.model.dtos.FranchiseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;



// create a mapper interface using mapstruct and dtos
@Mapper(componentModel = "spring")
@Component
public interface FranchiseMapper {

    FranchiseMapper INSTANCE = Mappers.getMapper(FranchiseMapper.class);

    // map the corresponding fields
    @Mapping(source = "movies", target = "movieIds")
    FranchiseDTO toDto(Franchise franchise);


    // default methods for mapping
    default List<FranchiseDTO> toDtoList(List<Franchise> franchises){
        return franchises.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    default List<Long> map(Set<Movie> value){
        return value.stream()
                .map(Movie::getId)
                .collect(Collectors.toList());
    }

    default Franchise toEntity(FranchiseDTO franchiseDTO){
        Franchise franchise = new Franchise();
        franchise.setName(franchiseDTO.getName());
        franchise.setDescription(franchiseDTO.getDescription());
        return franchise;
    }
}