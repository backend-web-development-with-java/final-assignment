package com.example.finalassignment.Runner;


import com.example.finalassignment.Repository.CharacterRepository;
import com.example.finalassignment.Repository.FranchiseRepository;
import com.example.finalassignment.Repository.MovieRepository;
import com.example.finalassignment.model.Franchise;
import com.example.finalassignment.model.Movie;
import com.example.finalassignment.model.Character;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MovieFranchiseCharacterRunner implements CommandLineRunner {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private CharacterRepository characterRepository;

    // populate the Character_table
    @Override
    public void run(String... args) throws Exception {

        // create new franchise
        Franchise LOTR = new Franchise();
        LOTR.setName("The Lord of the Rings");
        LOTR.setDescription("We follow Frodo and his friends on a journey to destroy the one ring to rule them all!");
        franchiseRepository.save(LOTR);


        // create the three movies in the franchise
        Movie movie1 = new Movie();
        movie1.setMovieTitle("The Fellowship of the Ring");
        movie1.setGenre("Fantasy");
        movie1.setReleaseYear("2001");
        movie1.setDirector("Peter Jackson");
        movie1.setPicture("https://www.example.com/fellowship.jpg");
        movie1.setTrailer("https://www.example.com/fellowship-trailer.mp4");
        movie1.setFranchise(LOTR);
        movieRepository.save(movie1);


        Movie movie2 = new Movie();
        movie2.setMovieTitle("The Two Towers");
        movie2.setGenre("Fantasy");
        movie2.setReleaseYear("2002");
        movie2.setDirector("Peter Jackson");
        movie2.setPicture("https://www.example.com/two-towers.jpg");
        movie2.setTrailer("https://www.example.com/two-towers-trailer.mp4");
        movie2.setFranchise(LOTR);
        movieRepository.save(movie2);

        Movie movie3 = new Movie();
        movie3.setMovieTitle("The Return of the King");
        movie3.setGenre("Fantasy");
        movie3.setReleaseYear("2003");
        movie3.setDirector("Peter Jackson");
        movie3.setPicture("https://www.example.com/return-of-the-king.jpg");
        movie3.setTrailer("https://www.example.com/return-of-the-king-trailer.mp4");
        movie3.setFranchise(LOTR);
        movieRepository.save(movie3);


        Character aragorn = new Character();
        aragorn.setFullName("Aragorn");
        aragorn.setAlias("Strider, Elessar, King of Gondor");
        aragorn.setGender("Male");
        aragorn.setPicture("https://example.com/aragorn.jpg");
        characterRepository.save(aragorn);


        Character legolas = new Character();
        legolas.setFullName("Legolas");
        legolas.setAlias("Archer");
        legolas.setGender("Male");
        legolas.setPicture("https://example.com/faramir.jpg");
        characterRepository.save(legolas);


        Character gandalf = new Character();
        gandalf.setFullName("Gandalf");
        gandalf.setAlias("Mithrandir, Gandalf the Grey, Gandalf the White");
        gandalf.setGender("Male");
        gandalf.setPicture("https://example.com/gandalf.jpg");
        characterRepository.save(gandalf);


        // add movies to characters
        gandalf.addMovie(movie1);
        legolas.addMovie(movie1);
        aragorn.addMovie(movie1);
        characterRepository.save(gandalf);
        characterRepository.save(legolas);
        characterRepository.save(aragorn);


        // add characters to movies
        movie1.addCharacter(aragorn);
        movie1.addCharacter(legolas);
        movie1.addCharacter(gandalf);
        movieRepository.save(movie1);


// uncomment code to delete the insertions
//        movieRepository.deleteAll();
//        franchiseRepository.deleteAll();
//        characterRepository.deleteAll();
    }
}