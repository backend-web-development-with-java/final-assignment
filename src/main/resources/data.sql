

-- Had issues with my postgres data source connection so I inserted data through a Runner class (with CommandLineRunner implementation) instead.
-- Insertions in postgresql would look something like this:


-- INSERT INTO character_table VALUES(full_name, alias, gender, picture) VALUES ('Aragorn', 'Strider', 'Male', 'https://example.com/Aragorn.jpg');
-- INSERT INTO character_table VALUES(full_name, alias, gender, picture) VALUES ('Gandalf', 'Gandalf the grey', 'Male', 'https://example.com/gandalf.jpg');
-- INSERT INTO character_table VALUES(full_name, alias, gender, picture) VALUES ('Legolas', 'Elf', 'Male', 'https://example.com/legolas.jpg');
-- INSERT INTO character_table VALUES(full_name, alias, gender, picture) VALUES ('Faramir', 'Archer', 'Male', 'https://example.com/faramir.jpg');
-- INSERT INTO character_table VALUES(full_name, alias, gender, picture) VALUES ('Gimli', 'Dwarf', 'Male', 'https://example.com/gimli.jpg');

--  ... Then a similar pattern to the fields created in the entity classes